﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using caalhp.IcePluginAdapters;
using CareStorePictureFrame.Commands;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;

namespace CareStorePictureFrame.ViewModels
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<Image> _images = new ObservableCollection<Image>();
        private string _pictureFolderPath;
        private Image _currentImage;
        private int _currentImageNumber;
        private ICommand _clickCommand;
        private bool _canExecute;

        private AppAdapter _adapter;
        private PictureFrameImplementation _imp;

        public Image CurrentImage
        {
            set
            {
                _currentImage = value;
                OnPropertyChanged("CurrentImage");

                
            }
            get
            {
                return _currentImage;
            }
        }

        public MainWindowViewModel()
        {
            ConnectToCaalhp();

            LoadPicturesOnSetupTimer();
        }

        private void LoadPicturesOnSetupTimer()
        {
            int pictureChangeInterval;
            //string path = Path.GetDirectoryName(
            //         Assembly.GetAssembly(typeof(MyClass)).CodeBase);
            var dirPath = ConfigurationManager.AppSettings["pictureFolderPath"];
            var pictureFolders = Directory.GetDirectories(Environment.CurrentDirectory, dirPath, SearchOption.AllDirectories);
            if (pictureFolders != null && pictureFolders.Length > 0)
                _pictureFolderPath = pictureFolders[0];

            // Is pictureChangeInterval an int, if yes continue, else display error
            bool isPictureChangeIntervalInt = int.TryParse(ConfigurationManager.AppSettings["pictureChangeInterval"], out pictureChangeInterval);

            if (isPictureChangeIntervalInt)
            {
                _canExecute = true;

                CurrentImage = new Image();
                _currentImageNumber = 1;
                GetImagesIntoAList();
                Rotate();


                // Start Slideshow by calling rotate() from DispatcherTimer Event
                DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
                dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
                dispatcherTimer.Interval = new TimeSpan(0, 0, pictureChangeInterval);
                dispatcherTimer.Start();
            }
            else
            {
                MessageBox.Show(
                    "App.config contains a faulty value. pictureFrameInterval value should be an int. It is: " +
                    ConfigurationManager.AppSettings["pictureChangeInterval"]);
            }

        }

        private void ConnectToCaalhp()
        {
            const string endpoint = "localhost";
            try
            {
                _imp = new PictureFrameImplementation(this);
                _adapter = new AppAdapter(endpoint, _imp);
            }
            catch (Exception e)
            {
                System.Media.SystemSounds.Asterisk.Play();
            }
        }

        private void GetImagesIntoAList()
        {
            _images = new ObservableCollection<Image>();

            if (!Directory.Exists(_pictureFolderPath))
            {
                Directory.CreateDirectory(_pictureFolderPath);
            }

            DirectoryInfo dir = new DirectoryInfo(_pictureFolderPath);
            FileInfo[] files = dir.GetFiles();
            if (!files.Any())
            {
                //MessageBox.Show("No pictures in picture folder. Path is: " + _pictureFolderPath);
            }
            else
            {
                foreach (var item in files)
                {
                    FileStream stream = new FileStream(item.FullName, FileMode.Open, FileAccess.Read);
                    Image i = new Image();
                    BitmapImage src = new BitmapImage();
                    src.BeginInit();
                    src.StreamSource = stream;
                    src.EndInit();
                    i.Source = src;
                    _images.Add(i);
                }
            }

        }

        private void Rotate()
        {
            if (_images.Any())
            {
                if (_currentImageNumber >= _images.Count - 1)
                {
                    _currentImageNumber = 0;
                    CurrentImage = _images[_currentImageNumber];
                }
                else
                {
                    _currentImageNumber++;
                    CurrentImage = _images[_currentImageNumber];
                }
            }
        }

        public ICommand ClickCommand
        {
            get
            {
                return _clickCommand ?? (_clickCommand = new PictureClickCommand(() => MyAction(), _canExecute));
            }
        }

        private void DoubleClickAction()
        {
            //System.Media.SystemSounds.Beep.Play();

            _imp.ReportHideTopMenuEvent();

//            var command = new caalhp.Core.Events.HideTop
  //          _adapter.HostProxy.ReportEvent(
            


        }

            //        var command = new Caalhp.Core.Events.
            //{
            //    Info = "CloseCAALHP",
            //    CallerName = "TopMenu",
            //    //CallerProcessId = _topMenuImplementation._processId,
            //};
            //var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);

            //_host.ReportEvent(serializedCommand);


        public void MyAction()
        {
            //System.Diagnostics.Debugger.Launch();

            if (!CheckForDoubleClick())
            {
                Rotate();
                if (topMenuHidden) ShowTopMenu();
             
            }
            else
            {
                DoubleClickAction();
                topMenuHidden = true;
            }

        }

        private void ShowTopMenu()
        {
            _imp.ReportShowTopMenuEvent();
        }

        private bool CheckForDoubleClick()
        {
            if (lastClick != null && lastClick.AddMilliseconds(500) > DateTime.Now) return true;

            lastClick = DateTime.Now;

            return false;
        }

        #region Dispatcher Event Handler

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            Rotate();
        }

        #endregion

        #region PropertyChanged Snippet
        public event PropertyChangedEventHandler PropertyChanged;
        private DateTime lastClick;
        private bool topMenuHidden;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion


    }
}
