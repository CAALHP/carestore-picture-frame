﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using caalhp.Core.Contracts;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using caalhp.Core.Utils.Helpers.Serialization.JSON;
using CareStorePictureFrame.ViewModels;
using CareStorePictureFrame.Views;
using System.Diagnostics;
using gma.System.Windows;
using System.Windows.Forms;
using caalhp.IcePluginAdapters.WPF;

namespace CareStorePictureFrame
{
    class PictureFrameImplementation : caalhp.Core.Contracts.IAppCAALHPContract
    {
        // Test commit
        private IAppHostCAALHPContract _host { get; set; }
        private Thread _thread;
        private int _processId;
        private MainWindow _currentWindow;
        private MainWindowViewModel _main;
        private volatile System.Windows.Application _app;
        private const string AppName = "PictureFrame";
        private System.Timers.Timer timer;
        private UserActivityHook actHook;
        private DateTime lastUserActivity;

        public PictureFrameImplementation(MainWindowViewModel main)
        {
            _main = main;

            MonitorUserAcitivty();

          
    
        }

        private void MonitorUserAcitivty()
        {
            //Debugger.Launch();

            actHook = new UserActivityHook(); // crate an instance with global hooks
            // hang on events
            actHook.OnMouseActivity += new MouseEventHandler(UserMouseActivityDetected);
            actHook.KeyDown += new KeyEventHandler(UserActivityDetected);
            //actHook.KeyPress += new KeyPressEventHandler(UserActivityDetected);
            actHook.KeyUp += new KeyEventHandler(UserActivityDetected);
            actHook.Start();

            timer = new System.Timers.Timer(2000);
            timer.Elapsed += timerCallback;
            timer.Enabled = true;
        }

        private void timerCallback(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (lastUserActivity != null && lastUserActivity.AddSeconds(120) < DateTime.Now)
                //System.Media.SystemSounds.Asterisk.Play();
                Show();
            
        }

        
        void MainFormLoad(object sender, System.EventArgs e)
        {
           
        }

        private void UserActivityDetected(object sender, KeyEventArgs e)
        {
            lastUserActivity = DateTime.Now;
        }

        public void UserMouseActivityDetected(object sender, MouseEventArgs e)
        {
            lastUserActivity = DateTime.Now;
        }
		
	

        //private void GetCPUCounter()
        //{

           
        //    PerformanceCounter cpuCounter = new PerformanceCounter();
        //    cpuCounter.CategoryName = "Processor";
        //    cpuCounter.CounterName = "% Processor Time";
        //    cpuCounter.InstanceName = "_Total";

        //    // will always start at 0
        //    dynamic firstValue = cpuCounter.NextValue();
        //    System.Threading.Thread.Sleep(1000);
        //    // now matches task manager reading
        //    dynamic secondValue = cpuCounter.NextValue();

        //    if (secondValue < 10)
        //    {

        //    }

        //}

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(ShowAppEvent e)
        {
            if (e.AppName.Equals(AppName))
            {
                //Show homescreen
                Show();
            }
        }

        public string GetName()
        {
            return AppName;
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
           // _app.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
            System.Windows.Application.Current.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;

            // Subscribe to ShowAppEvents
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId);
        }

        private void DispatchToApp(Action action)
        {
            System.Windows.Application.Current.Dispatcher.Invoke(action);
        }

        public void Show()
        {
            DispatchToApp(() =>
            {
                //DoTheMessagePump();
                var window = System.Windows.Application.Current.MainWindow;
                if (!window.IsVisible)
                {
                    window.Show();
                }
                window.WindowState = WindowState.Maximized;
                window.Activate();
                window.Topmost = true;
                window.Topmost = false;
                window.Focus();
                //DoTheMessagePump();
                Helper.BringToFront();
            });
        }

        internal void ReportHideTopMenuEvent()
        {
            var command = new HideTopMenuEvent();
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
            _host.Host.ReportEvent(serializedCommand);

        }

        internal void ReportShowTopMenuEvent()
        {
            var command = new ShowTopMenuEvent();
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
            _host.Host.ReportEvent(serializedCommand);
        }
    }
}
