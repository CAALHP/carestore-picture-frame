﻿using System;
using System.Windows.Input;

namespace CareStorePictureFrame.Commands
{
    public class PictureClickCommand : ICommand
    {
        private Action _action;
        
        private bool _canExecute;
        private DateTime lastClick;

        public PictureClickCommand(Action action,  bool canExecute)
        {
            _action = action;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute;
        }

        public void Execute(object parameter)
        {
            
            _action();
        }

        public event EventHandler CanExecuteChanged;

    }
}
