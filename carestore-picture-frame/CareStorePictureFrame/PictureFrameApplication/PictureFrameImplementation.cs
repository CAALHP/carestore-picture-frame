﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using CareStorePictureFrame;

namespace Plugins.PictureFrame
{
    class PictureFrameImplementation : CAALHP.Contracts.IAppCAALHPContract
    {
        private IAppHostCAALHPContract _host { get; set; }
        private Thread _thread;
        private int _processId;
        private MainWindow _currentWindow;
        private volatile Application _app;
        private const string AppName = "PictureFrame";

        public PictureFrameImplementation()
        {
            InitThread();

            DispatchToApp(() =>
            {
                _currentWindow = new MainWindow();
                _currentWindow.Hide();
            });
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(ShowAppEvent e)
        {
            if (e.AppName.Equals(AppName))
            {
                //Show homescreen
                Show();
            }
        }

        public string GetName()
        {
            return AppName;
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;

            // Subscribe to ShowAppEvents
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId);
        }

        private void InitThread()
        {
            _thread = new Thread(() =>
            {
                _app = new Application();
                _app.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                _app.Run();
            });

            //This is needed for GUI threads
            _thread.SetApartmentState(ApartmentState.STA);
            // Make the thread a background thread
            //_thread.IsBackground = true;

            _thread.Start();
        }

        public void Show()
        {
            DispatchToApp(() =>
            {
                _currentWindow.Show();
                _currentWindow.Activate();
            });
        }

        private void DispatchToApp(Action action)
        {
            _app.Dispatcher.Invoke(action);
        }
    }
}
