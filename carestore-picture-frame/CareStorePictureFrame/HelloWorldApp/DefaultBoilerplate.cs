﻿using System.AddIn;
using Plugins.AppPluginAdapters;

namespace Plugins.WpfTemplateProject
{
    [AddIn("TemplateProject App", Version = "1.0.0.0")]                     // Todo: Change Application name and version
    public class PictureFrameBoilerplate : AppPluginAdapter
    {
        public PictureFrameBoilerplate()
        {   
            App = new DefaultImplementation();                              // Todo: If the implementation class-name is changed, change it here aswel.
        }
    }
}
