﻿using WpfImplementationLibrary;

namespace Plugins.WpfTemplateProject                                // Todo: Change namespace to Plugins.<ApplicationName>
{                                                                   // Can be done in Project Properties -> Assembly name & Default namespace
    public class DefaultImplementation : AbstractWpfImplementation
    {
        private MainWindow _currentWindow;                          // Todo: Change Window "MainWindow" to the Window you want to launch at app-startup

        public DefaultImplementation()                              // Todo: Implement whatever you want your application to do at app-startup
        {
            DispatchToApp(() =>
            {
                _currentWindow = new MainWindow();
                _currentWindow.Hide();
            });
        }


        public override string GetName()
        {
            return "MessageCenter";                                 // Todo: Change the return strings value to your applications name
        }

        public override void Show()                                 // Todo: Implement what should happen when the application gets a Show event
        {
            DispatchToApp(() =>
            {
                _currentWindow.Show();
                _currentWindow.Activate();
            });
        }
    }
}