﻿using System;
using CAALHP.Contracts;
using Moq;
using Plugins.PictureFrame;

namespace PictureFrameConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var impl = new PictureFrameImplementation();
            var host = new Mock<IHostCAALHPContract>().Object;
            impl.Initialize(host, 0);
            impl.Show();
            Console.ReadLine();
        }
    }
}
